import { Injectable } from '@angular/core';
import { URL_ROOT } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiHospitalsCDMXService {
  constructor(private http: HttpClient) {}

  getHospitals() {
    return this.http
      .get(`${URL_ROOT}/?dataset=hospitales-y-centros-de-salud&q=&rows=27`)
      .pipe(map((response: any) => response.records));
  }
}
