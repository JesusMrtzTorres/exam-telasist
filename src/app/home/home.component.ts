import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Hospital } from '../../interfaces/hospital.interface';
import { ApiHospitalsCDMXService } from '../services/api-hospitals-cdmx.service';

declare let L: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  hospitals: Hospital[] = [];

  constructor(private apiHospitalsCDMXservice: ApiHospitalsCDMXService) {}

  ngOnInit() {
    const map = L.map('map').setView([19.42847, -99.12766], 13);

    this.apiHospitalsCDMXservice.getHospitals().subscribe((hospitalsResponse: Hospital[]) => {
      this.hospitals = hospitalsResponse;

      L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
        attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      }).addTo(map);

      this.hospitals.forEach((hospital: Hospital) => {
        const marker = L.marker([hospital.fields.latitud, hospital.fields.longitud]).addTo(map);
        marker
          .bindPopup(
            `<p>Nombre: <span>${hospital.fields.nombre}</span></p>
        <p>Titular: <span>${hospital.fields.titular}</span></p>`
          )
          .openPopup();
      });
    });
  }
}
