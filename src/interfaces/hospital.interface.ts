import { Type } from '@angular/core';
export interface Hospital {
  datasetid: string;
  fields: {
    coordenadas: string;
    geopoint: any;
    latitud: number;
    longitud: number;
    nombre: string;
    titular: string;
  };
  geometry: {
    coordinates: any;
    type: string;
  };
  record_timestamp: string;
  recordid: string;
}
